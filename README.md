## Exercices de génétique humaine

Ces exercices ont pour objectif de faciliter l'entraînement sur les notions d'hérédité dans le cadre des apprentissage en génétique humaine.

Ils sont conçus pour être simples au niveau de l'expérience utilisateur, mais aussi simples au niveau du code utilisé (HTML + JavaScript).

Le code peut ainsi être repris simplement pour créer de nouveaux exercices similaires sur d'autres arbres généalogiques (pour des corrections de sujets d'examen par exemple).


## Avenir de ces exercices

Ces pages (exercices guidés et solutions) s'avèrent extrèmement pratiques dans le cadre de l'apprentissage des principales lois de l'hérédité.

Il est encore prévu d'ajouter la fonctionnalité de fond sombre/clair qui est présent dans Bootstrap 5.3.

Toute bonne volonté est la bienvenue sur ce projet pour en faciliter encore plus l'accès et la diffusion

## GitLab Site

Les pages étant en full html on peut les consulter directement à cette adresse https://bio.forge.apps.education.fr/heredite/ 

## Capture d'écran

![Capture d'écran de la version 2024.04.28](public/images/capture_phenylceto_bootstrap.png "Capture d'écran de la version 2024.05.02")

## Fork d'un travail de Stéphane Rabouin

Cet ensemble de pages a été, à l'origine, créé par Stéphane Rabouin, professeur de SVT et co-auteur de nombreux manuels scolaires. Ces anciens documents sont consultables à l'adresse suivante : http://svt.tice.ac-orleans-tours.fr/php5/publis/genetique/sommair.htm

Stéphane Rabouin n'a pas hésité pour passer ses documents originaux sous la licence CC-BY.

Les documents d'origine ont d'abord été simplement nettoyés pour n'en ressortir que le code html simple et le code JS. Ensuite, une feuille de style est venue par dessus pour mettre en page l'ensemble des documents.

Quelques erreurs correspondants essentiellement à des changements dans les conventions d'écriture des génotypes ont également étés corrigées.

## Contributeurs

### Stéphane Rabouin
- professeur certifié de SV-STU sur l'académie d' Orléans-Tours et co-rédacteur de nombreux manuels scolaires de SVT
- première création de ces exercices http://svt.tice.ac-orleans-tours.fr/php5/publis/genetique/sommair.htm
- passage du code source sous la licence CC-BY
- correction des conventions d'écriture des génotypes pour les actualiser.

### Patrice Hardouin
- professeur certifié de Biotechnologies Santé-Environnement et professeur particulier en Biologie notamment https://patrice.biotechno.fr
- nettoyage du code d'origine, mise en place des CSS de Boostrap + petite CSS personnalisée
- redistribution des informations pour un meilleur accès et une plus grande efficacité pour la résolution des exercices et l'apprentissage des notions liées
- ajout d'une page d'accueil illustrée et contextualisée
- re-rédaction de certaines solutions d'exercices pour éviter d'éventuelles erreurs de compréhension des apprenants

